﻿using Pr0gramm.Client;
using Pr0gramm.JSON;
using Pr0grammLib.Config;
using Pr0grammLib.DB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Pr0grammCrowler
{
    class Pr0gramm
    {
        private static Config m_config = null;

        #region Tools

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public static String getDay(double unixTimeStamp)
        {
            DateTime _unixTime = UnixTimeStampToDateTime(unixTimeStamp);
            String _time = _unixTime.ToShortDateString();
            _time = _time.Replace("-", "_");
            _time = _time.Replace("_", "_");
            _time = _time.Replace(":", "_");
            _time = _time.Replace("/", "_");
            _time = _time.Replace("\\", "_");
            _time = _time.Replace(" ", "");
            _time = _time.Replace("AM", "");
            _time = _time.Replace("PM", "_");
            return _time;
        }

        #endregion

        #region Client

        private static List<Item> getItems(int _flag, bool _promoted)
        {
            try
            {
                return Pr0grammClient.getPageContend(_flag, _promoted).PageItems;
            }
            catch
            {
                return new List<Item>();
            }
        }

        #endregion

        static void Main(string[] args)
        {
            m_config = new Config(new FileInfo("Config.ini"));

            int _lastRun = 0;

            while (true)
            {
                int _now = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

                if ((_lastRun + (new Random().Next(180, 300))) < _now)
                {
                    run();
                    _lastRun = _now;
                }

                Thread.Sleep(100);
            }

        }

        private static void run()
        {
            try
            {
                if (!Directory.Exists("data"))
                    Directory.CreateDirectory("data");

                if (!Directory.Exists("data/full"))
                    Directory.CreateDirectory("data/full");

                if (!Directory.Exists("data/meta"))
                    Directory.CreateDirectory("data/meta");

                if (!Directory.Exists("data/mini"))
                    Directory.CreateDirectory("data/mini");

                int _requiredLikes = int.Parse(m_config.get("General", "RequiredLikes", "0"));

                foreach (Item _thisItem in getItems(9, true))
                {
                    if (_thisItem.VoteUP <= _requiredLikes)
                        continue;

                    String _FileNameFull = _thisItem.ItemID + "." + _thisItem.Type;
                    String _FileNameMini = _thisItem.ItemID + ".jpg";

                    String _rawFileDirectoryPath = "data/full/";
                    String _rawThumpDirectoryPath = "data/mini/";

                    String _FileDateDirectoryPath = _rawFileDirectoryPath + getDay(_thisItem.CreatedTime) + "/";
                    String _ThumpDateDirectoryPath = _rawThumpDirectoryPath;

                    String _FileDirectoryPath = Path.Combine(_FileDateDirectoryPath, _thisItem.Type + "/");
                    String _ThumpDirectoryPath = Path.Combine(_ThumpDateDirectoryPath, _thisItem.Type + "/");

                    String _FilePath = Path.Combine(_FileDirectoryPath, _FileNameFull);
                    String _ThumpPath = Path.Combine(_ThumpDirectoryPath, _FileNameMini);

                    if (!Directory.Exists(_FileDateDirectoryPath))
                        Directory.CreateDirectory(_FileDateDirectoryPath);

                    if (!Directory.Exists(_ThumpDateDirectoryPath))
                        Directory.CreateDirectory(_ThumpDateDirectoryPath);

                    if (!Directory.Exists(_FileDirectoryPath))
                        Directory.CreateDirectory(_FileDirectoryPath);

                    if (!Directory.Exists(_ThumpDirectoryPath))
                        Directory.CreateDirectory(_ThumpDirectoryPath);

                    if (!File.Exists(_FilePath))
                    {

                        Console.Write("Download Item " + _thisItem.ItemID + "...");

                        Pr0grammClient.DownloadRawFile(new Uri("https://img.pr0gramm.com/" + _thisItem.ImageURL.Replace(@"\/", "/")), new FileInfo(_FilePath + ".temp"));

                        if (File.Exists(_FilePath + ".temp"))
                            File.Move(_FilePath + ".temp", _FilePath);

                        Console.WriteLine(" ...finish");
                    }

                    if (!File.Exists(_ThumpPath))
                    {
                        Console.Write("Download Thump " + _thisItem.ItemID + "...");

                        Pr0grammClient.DownloadRawFile(new Uri("https://thumb.pr0gramm.com/" + _thisItem.ThumpURL.Replace(@"\/", "/")), new FileInfo(_ThumpPath + ".temp"));

                        if (File.Exists(_ThumpPath + ".temp"))
                            File.Move(_ThumpPath + ".temp", _ThumpPath);

                        Console.WriteLine(" ...finish");
                    }

                    if(!DBItemEntry.exist(_thisItem.ItemID))
                    {
                        Console.Write("Create DB Entry for " + _thisItem.ItemID + "...");
                        _thisItem.ItemInfo = Pr0grammClient.getItemInfo(_thisItem);
                        DBItemEntry.save(_thisItem);
                        Console.WriteLine(" ...finish");
                    }

                    Thread.Sleep(new Random().Next(8000, 27000));
                }
            }
            catch(Exception _error)
            {
                Console.WriteLine("Error while run(): " + _error.Message);
                Console.WriteLine(_error.StackTrace);
            }
        }
    }
}
