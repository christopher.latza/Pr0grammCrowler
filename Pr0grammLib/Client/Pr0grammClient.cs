﻿using Newtonsoft.Json;
using Pr0gramm.JSON;
using System;
using System.IO;
using System.Net;

namespace Pr0gramm.Client
{
    public class Pr0grammClient
    {
        private static WebClient m_client = new WebClient();
        private static string makeHTTPCall(String _url)
        {
            m_client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");

            IWebProxy _proxy = WebRequest.DefaultWebProxy;
            if (_proxy != null)
            {
                _proxy.Credentials = CredentialCache.DefaultCredentials;
                m_client.Proxy = _proxy;
            }

            byte[] raw = m_client.DownloadData(_url);
            return System.Text.Encoding.UTF8.GetString(raw);
        }

        #region PageContend

        public static PageList getPageContend(int _flag, bool _promoted)
        {
            String _baseURL = "https://pr0gramm.com/api/items/get?flags=" + _flag;

            if (_promoted)
                _baseURL += _baseURL + "&promoted=1";

            return getPageContend(_baseURL);
        }

        private static PageList getPageContend(string _url)
        {
            PageList _pageContend = JsonConvert.DeserializeObject<PageList>(makeHTTPCall(_url));

            return _pageContend;
        }

        #endregion

        #region ItemInfo
        public static Info getItemInfo(Item _item)
        {
            Info _info = getItemInfo(_item.ItemID);

            foreach (Comment _c in _info.ItemComments)
                _c.Item = _item;

            foreach (Tag _t in _info.ItemTags)
                _t.Item = _item;

            return _info;
        }

        private static Info getItemInfo(int _id)
        {
            String _baseURL = "https://pr0gramm.com/api/items/info?itemId=" + _id;

            return getItemInfo(_baseURL);
        }

        private static Info getItemInfo(string _url)
        {
            Info _info = null;

            try
            {
                _info = JsonConvert.DeserializeObject<Info>(makeHTTPCall(_url));
            }catch
            {
                Console.WriteLine("Error while deserialize object data.");
            }

            return _info;
        }
        #endregion

        #region

        public static void DownloadRawFile(Uri _uri, FileInfo _file)
        {
            try
            {
                m_client.DownloadFile(_uri, _file.FullName);
            }catch
            {
                Console.WriteLine("Error while download file.");
            }
        }

        #endregion
    }
}
