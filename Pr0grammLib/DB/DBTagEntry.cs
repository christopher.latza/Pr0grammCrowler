﻿using Pr0gramm.JSON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pr0grammLib.DB
{
    public class DBTagEntry
    {
        public static void save(Item _item)
        {
            if (_item == null)
                return;

            if (_item.ItemInfo == null)
                return;

            foreach (Tag _t in _item.ItemInfo.ItemTags)
                save(_t);
        }

        public static void save(Info _infoData)
        {
            if (_infoData == null)
                return;

            foreach (Tag _t in _infoData.ItemTags)
                save(_t);
        }

        public static void save(Tag _tagData)
        {
            if (_tagData == null)
                return;

            DBHandler.noSQLQuerry("INSERT INTO Tags (TagID, Content, ItemID) VALUES (@TagID, @Content, @ItemID)", new string[][] {
                new string[] { "@TagID", _tagData.TagID.ToString() },
                new string[] { "@Content", _tagData.Content.ToString() },
                new string[] { "@ItemID", _tagData.Item.ItemID.ToString() }
            });
        }
    }
}
