﻿using Pr0gramm.JSON;
using System.Data.SQLite;

namespace Pr0grammLib.DB
{
    public class DBItemEntry
    {
        public static void save(PageList _pageData)
        {
            if (_pageData == null)
                return;

            foreach (Item _thisItem in _pageData.PageItems)
                save(_thisItem);
        }

        public static void save(Item _itemData)
        {
            if (_itemData == null)
                return;

            if (exist(_itemData.ItemID))
                return;

            DBHandler.noSQLQuerry("INSERT INTO Items (ItemID, SeitenID, VoteUP, VoteDown, CreatedTime, ImageURL, ThumpURL, FullsizeURL, Width, Height, Audio, Source, Flags, User, Mark) VALUES (@ItemID, @SeitenID, @VoteUP, @VoteDown, @CreatedTime, @ImageURL, @ThumpURL, @FullsizeURL, @Width, @Height, @Audio, @Source, @Flags, @User, @Mark)",
                new string[][] {
                    new string[] { "@ItemID", _itemData.ItemID.ToString() },
                    new string[] { "@SeitenID", _itemData.SeitenID.ToString() },
                    new string[] { "@VoteUP", _itemData.VoteUP.ToString() },
                    new string[] { "@VoteDown", _itemData.VoteDown.ToString() },
                    new string[] { "@CreatedTime", _itemData.CreatedTime.ToString() },
                    new string[] { "@ImageURL", _itemData.ImageURL.ToString() },
                    new string[] { "@ThumpURL", _itemData.ThumpURL.ToString() },
                    new string[] { "@FullsizeURL", _itemData.FullsizeURL.ToString() },
                    new string[] { "@Width", _itemData.Width.ToString() },     
                    new string[] { "@Height", _itemData.Height.ToString() },
                    new string[] { "@Audio", _itemData.Audio.ToString() },
                    new string[] { "@Source", _itemData.Source.ToString() },
                    new string[] { "@Flags", _itemData.Flags.ToString() },
                    new string[] { "@User", _itemData.User.ToString() },
                    new string[] { "@Mark", _itemData.Mark.ToString() }
                });

            if (_itemData.ItemInfo != null)
                DBCommentEntry.save(_itemData.ItemInfo);

            if (_itemData.ItemInfo != null)
                DBTagEntry.save(_itemData.ItemInfo);
        }

        public static Item get(int _itemID)
        {
            SQLiteDataReader _result = DBHandler.SQLQuerry("SELECT ItemID,SeitenID,VoteUP,VoteDown,CreatedTime,ImageURL,ThumpURL,FullsizeURL,Width,Height,Audio,Source,Flags,User,Mark FROM Items WHERE ItemID = @ItemID LIMIT 1", new string[][] { new string[] { "@ItemID", _itemID.ToString()} });

            if (_result.HasRows)
            {
                Item _returnItem = new Item();
                _returnItem.Audio = false;

                while (_result.Read())
                {
                    _returnItem.ItemID = int.Parse(_result.GetString(0));
                    _returnItem.SeitenID = int.Parse(_result.GetString(1));
                    _returnItem.VoteUP = int.Parse(_result.GetString(2));
                    _returnItem.VoteDown = int.Parse(_result.GetString(3));
                    _returnItem.CreatedTime = int.Parse(_result.GetString(4));
                    _returnItem.ImageURL = _result.GetString(5);
                    _returnItem.ThumpURL = _result.GetString(6);
                    _returnItem.FullsizeURL = _result.GetString(7);
                    _returnItem.Width = int.Parse(_result.GetString(8));
                    _returnItem.Height = int.Parse(_result.GetString(9));

                    if (_result.GetString(10).Trim().ToLower() == "true")
                        _returnItem.Audio = true;

                    _returnItem.Source = _result.GetString(11);
                    _returnItem.Flags = int.Parse(_result.GetString(12));
                    _returnItem.User = _result.GetString(13);
                    _returnItem.Mark = int.Parse(_result.GetString(14));

                    return _returnItem;
                }

                return _returnItem;
            }
            else
            {
                return null;
            }
        }

        public static bool exist(int _itemID)
        {
            if (get(_itemID) != null)
                return true;

            return false;
        }
    }
}
