﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;

namespace Pr0grammLib.DB
{
    class DBHandler
    {
        private static SQLiteConnection m_SQLConnection = null;
        private static bool m_defaultCreateFinish = false;

        private static void openDBConnection()
        {
            if (m_SQLConnection == null)
            {
                m_SQLConnection = new SQLiteConnection();
                m_SQLConnection.ConnectionString = "Data Source=Daten.db";
                m_SQLConnection.Open();
                createDefaultTables();
            }
        }

        private static void CloseDBConnection()
        {
            if (m_SQLConnection != null)
            {
                try
                {
                    m_SQLConnection.Close();
                }catch
                {
                    Console.WriteLine("Error while close the DB connection.");
                }

                m_SQLConnection = null;
            }
        }

        private static void createDefaultTables()
        {
            if(m_defaultCreateFinish != true)
            {
                m_defaultCreateFinish = true;
                noSQLQuerry("CREATE TABLE IF NOT EXISTS Items (ItemID VARCHAR(36) NOT NULL, SeitenID VARCHAR(36) NOT NULL, VoteUP VARCHAR(36) NOT NULL, VoteDown VARCHAR(36) NOT NULL, CreatedTime VARCHAR(36) NOT NULL, ImageURL VARCHAR(36) NOT NULL, ThumpURL VARCHAR(36) NOT NULL, FullsizeURL VARCHAR(36) NOT NULL, Width VARCHAR(36) NOT NULL, Height VARCHAR(36) NOT NULL, Audio VARCHAR(36) NOT NULL, Source VARCHAR(36) NOT NULL, Flags VARCHAR(36) NOT NULL, User VARCHAR(36) NOT NULL, Mark VARCHAR(36) NOT NULL);", new String[0][]);
                noSQLQuerry("CREATE TABLE IF NOT EXISTS Comments (ItemID VARCHAR(36) NOT NULL, CommentID VARCHAR(36) NOT NULL, ParentID VARCHAR(36) NOT NULL, User VARCHAR(36) NOT NULL, Content VARCHAR(36) NOT NULL, CreatedTime VARCHAR(36) NOT NULL, VoteUP VARCHAR(36) NOT NULL, VoteDown VARCHAR(36) NOT NULL);", new String[0][]);
                noSQLQuerry("CREATE TABLE IF NOT EXISTS Tags (TagID VARCHAR(36) NOT NULL, Content VARCHAR(36) NOT NULL, ItemID VARCHAR(36) NOT NULL);", new String[0][]);
            }
        }

        public static void noSQLQuerry(String _sqlCommand, String[][] _arguemts)
        {
            try
            {
                openDBConnection();

                SQLiteCommand _sqlLiteCommand = new SQLiteCommand(_sqlCommand, m_SQLConnection);

                foreach (String[] _par in _arguemts)
                    if (_par.Length == 2)
                        _sqlLiteCommand.Parameters.AddWithValue(_par[0], _par[1]);

                _sqlLiteCommand.ExecuteNonQuery();
                _sqlLiteCommand.Dispose();
            }catch(Exception _error)
            {
                Console.WriteLine(_error.Message);
                Console.WriteLine(_error.StackTrace);
                CloseDBConnection();
            }
        }

        public static SQLiteDataReader SQLQuerry(String _sqlCommand, String[][] _arguemts)
        {
            try
            {
                openDBConnection();

                SQLiteCommand _sqlLiteCommand = new SQLiteCommand(_sqlCommand, m_SQLConnection);

                foreach (String[] _par in _arguemts)
                    if (_par.Length == 2)
                        _sqlLiteCommand.Parameters.AddWithValue(_par[0], _par[1]);

                return _sqlLiteCommand.ExecuteReader();
            }catch(Exception _error)
            {
                Console.WriteLine(_error.Message);
                Console.WriteLine(_error.StackTrace);
                CloseDBConnection();
            }

            return null;
        }
    }
}
