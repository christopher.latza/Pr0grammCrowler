﻿using Pr0gramm.JSON;

namespace Pr0grammLib.DB
{
    public class DBCommentEntry
    {
        public static void save(Info _infoData)
        {
            if (_infoData == null)
                return;

            foreach (Comment _thisComment in _infoData.ItemComments)
                save(_thisComment);
        }

        public static void save(Comment _commentData)
        {
            if (_commentData == null)
                return;

            DBHandler.noSQLQuerry("INSERT INTO Comments (ItemID, CommentID, ParentID, User, Content, CreatedTime, VoteUP, VoteDown) VALUES (@ItemID, @CommentID, @ParentID, @User, @Content, @CreatedTime, @VoteUP, @VoteDown)", new string[][] {
                new string[] { "@ItemID", _commentData.Item.ItemID.ToString() },
                new string[] { "@CommentID", _commentData.Item.ItemID.ToString() },
                new string[] { "@ParentID", _commentData.ParentID.ToString() },
                new string[] { "@User", _commentData.User.ToString() },
                new string[] { "@Content", _commentData.Content.ToString() },
                new string[] { "@CreatedTime", _commentData.CreatedTime.ToString() },
                new string[] { "@VoteUP", _commentData.VoteUP.ToString() },
                new string[] { "@VoteDown", _commentData.VoteDown.ToString() }
            });
        }
    }
}
