﻿using Newtonsoft.Json;

namespace Pr0gramm.JSON
{
    public class Tag
    {
        [JsonProperty(PropertyName = "id")]
        public int TagID = 0;

        [JsonProperty(PropertyName = "tag")]
        public string Content = "";

        [JsonIgnore]
        public Item Item = null;
    }
}
