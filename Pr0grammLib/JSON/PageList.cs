﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Pr0gramm.JSON
{
    public class PageList
    {
        [JsonProperty(PropertyName = "atStart")]
        public bool FirstPage = false;

        [JsonProperty(PropertyName = "atEnd")]
        public bool LastPage = false;

        [JsonProperty(PropertyName = "error")]
        public string Error = "";

        [JsonProperty(PropertyName = "items")]
        public List<Item> PageItems = new List<Item>();
    }
}
