﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Pr0gramm.JSON
{
    public class Info
    {
        [JsonProperty(PropertyName = "tags")]
        public List<Tag> ItemTags = new List<Tag>();

        [JsonProperty(PropertyName = "comments")]
        public List<Comment> ItemComments = new List<Comment>();
    }
}
