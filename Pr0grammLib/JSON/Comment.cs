﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Pr0gramm.JSON
{
    public class Comment
    {
        [JsonProperty(PropertyName = "id")]
        public int CommentID = 0;

        [JsonProperty(PropertyName = "parent")]
        public int ParentID = 0;

        [JsonProperty(PropertyName = "name")]
        public string User = "";

        [JsonProperty(PropertyName = "content")]
        public string Content = "";

        [JsonProperty(PropertyName = "created")]
        public int CreatedTime = 0;

        [JsonProperty(PropertyName = "up")]
        public int VoteUP = 0;

        [JsonProperty(PropertyName = "down")]
        public int VoteDown = 0;

        [JsonIgnore]
        public Item Item = null;

    }
}
