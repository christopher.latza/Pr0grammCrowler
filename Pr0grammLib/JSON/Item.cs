﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace Pr0gramm.JSON
{
    public class Item
    {
        [JsonProperty(PropertyName = "id")]
        public int ItemID = 0;

        [JsonProperty(PropertyName = "promoted")]
        public int SeitenID = 0;

        [JsonProperty(PropertyName = "up")]
        public int VoteUP = 0;

        [JsonProperty(PropertyName = "down")]
        public int VoteDown = 0;

        [JsonProperty(PropertyName = "created")]
        public double CreatedTime = 0;

        [JsonProperty(PropertyName = "image")]
        public string ImageURL = "";

        [JsonProperty(PropertyName = "thumb")]
        public string ThumpURL = "";

        [JsonProperty(PropertyName = "fullsize")]
        public string FullsizeURL = "";

        [JsonProperty(PropertyName = "width")]
        public int Width = 0;

        [JsonProperty(PropertyName = "height")]
        public int Height = 0;

        [JsonProperty(PropertyName = "audio")]
        public bool Audio = false;

        [JsonProperty(PropertyName = "source")]
        public string Source = "";

        [JsonProperty(PropertyName = "flags")]
        public int Flags = 0;

        [JsonProperty(PropertyName = "user")]
        public string User = "";

        [JsonProperty(PropertyName = "mark")]
        public int Mark = 0;

        [JsonIgnore]
        public string FullFilePath = "";

        [JsonIgnore]
        public Info ItemInfo = null;

        [JsonIgnore]
        public string TumpFilePath = "";

        [JsonIgnore]
        public string Type
        {
            get
            {
                return Path.GetExtension(Path.Combine("images/", ImageURL)).Replace(".", "");
            }
        }
    }
}
