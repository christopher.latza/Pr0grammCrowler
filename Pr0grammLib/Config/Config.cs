﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Pr0grammLib.Config
{
    public class Config
    {
        private Dictionary<String, ConfigArea> m_areas = new Dictionary<String, ConfigArea>();

        /// <summary>
        /// Create a new ConfigFile object to read an ini file.
        /// </summary>
        /// <param name="file">An FileInfo object for the ini file</param>
        public Config(FileInfo file)
        {
            if (file == null)
                return;

            if (!file.Exists)
                return;

            String[] _fileContend = File.ReadAllLines(file.FullName);

            String _currentINIAreaName = null;
            ConfigArea _currentINIArea = null;

            foreach (String _fileLine in _fileContend)
            {
                String[] _lineSplit = null;

                if (_fileLine.Length == 0)
                    continue;

                if (_fileLine[0] == '[' && _fileLine[_fileLine.Length - 1] == ']')
                    _currentINIAreaName = _fileLine.Replace("[", "").Replace("]", "");

                if (_currentINIAreaName == null)
                    continue;

                _currentINIAreaName = _currentINIAreaName.Trim().ToLower();

                if (!m_areas.ContainsKey(_currentINIAreaName))
                    m_areas.Add(_currentINIAreaName, new ConfigArea());

                if (m_areas.ContainsKey(_currentINIAreaName))
                    m_areas.TryGetValue(_currentINIAreaName, out _currentINIArea);

                if (_currentINIArea == null)
                    continue;

                String _newLine = _fileLine;

                if (_newLine.Contains("\t"))
                    _newLine = _newLine.Replace("\t", "");

                if (_fileLine.Contains('='))
                    _lineSplit = _fileLine.Split('=');

                if (_lineSplit == null)
                    continue;

                String _currentAreaKeyName = _lineSplit[0].Trim().ToLower();

                if (_currentAreaKeyName.Length == 0)
                    continue;

                if (_lineSplit != null)
                    _currentINIArea.Set(_currentAreaKeyName, _lineSplit[1].Trim());
            }
        }

        public String get(String _requestedAreaName, String _requestedKeyName, String _default)
        {
            String _realAreaName = _requestedAreaName.Trim().ToLower();
            String _realKeyName = _requestedKeyName.Trim().ToLower();

            ConfigArea _currentINIArea = null;

            if (m_areas.ContainsKey(_realAreaName))
                m_areas.TryGetValue(_realAreaName, out _currentINIArea);

            if (_currentINIArea == null)
                return _default;

            return _currentINIArea.Get(_realKeyName);
        }
    }

    public class ConfigArea
    {
        private Dictionary<String, String> m_entrys = new Dictionary<String, String>();

        public void Set(String key, String value)
        {
            if (!m_entrys.ContainsKey(key))
            {
                m_entrys.Add(key, value);
            }
            else
            {
                m_entrys[key] = value;
            }

        }

        public String Get(String key)
        {
            String returnValue = null;

            if (m_entrys.ContainsKey(key))
                m_entrys.TryGetValue(key, out returnValue);

            return returnValue;
        }
    }

}
